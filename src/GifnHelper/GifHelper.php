<?php

namespace GifnHelper;

use Imagick;
use ImagickException;

/**
 * Class GifHelper
 *
 * @author Adam Boerema
 * @package helpers
 * @requirements Imagick 6.8
 */
class GifHelper{

    /**
     * @var array
     */
    protected $images;

    /**
     * @var Imagick $gif
     */
    protected $gif;

    /**
     * @var array
     */
    protected $frames = array();

    /**
     * @var $height
     */
    protected $height = 960;

    /**
     * @var $width
     */
    protected $width = 720;

    /**
     * @var $filepath
     */
    protected $filePath;

    /**
     * @var $overlay
     */
    protected $overlay = null;

    /**
     * @var $saveFrames
     */
    protected $saveFrames = false;


    /**
     * Constructor
     * Takes in an array of file paths or byte code
     *
     * @param array $images
     */
    public function __construct(array $images){
        $this->images = $images;
        $this->filePath = "/tmp/".rand().time().".gif";

        $this->gif = new Imagick();
        $this->gif->setformat("gif");
    }

    /**
     * Builds the gif and writes to file
     *
     * @returns boolean
     */
    public function save(){
        //Empty the frames
        $this->gif->clear();
        $this->frames = array();

        $imageCount = count($this->images);
        for($i=0; $i < $imageCount; $i++){
            $isSquare = ($this->height/$this->width == 1) ? true : false;
            $forceResize = !$isSquare; //Do not force resize squares

            $image = $this->images[$i];
            $file = $this->_findFile($image);

            try{
                $frame = new Imagick();

                //If not a file path read the byte code
                if($file){
                    $frame->readimagefile(fopen($file, "wb"));
                } else {
                    $frame->readimageblob(base64_decode($image));
                }

                //If the image is square
                if($isSquare) {
                    $frame = $this->_resizeImagick($frame, 'CENTER');
                }
                $frame->scaleImage($this->width, $this->height);
                $frame->setImagePage(0, 0, 0, 0);

                if($this->overlay){
                    $overlayImagick = new Imagick($this->_buildOverlayPath($this->overlay));

                    //Prep overlay for square if needed
                    if($isSquare){
                        $overlayImagick = $this->_resizeImagick($overlayImagick, 'BOTTOM');
                        $overlayImagick->thumbnailImage($this->width, $this->height, true, true);
                    }
                    $frame->setImageColorspace($overlayImagick->getImageColorspace());
                    $frame->compositeImage($overlayImagick, Imagick::COMPOSITE_DEFAULT, 0, 0);
                }

                //Save individual frame
                if($this->saveFrames){
                    $frame->writeImageFile(fopen($this->_buildFramePath($i), "wb"));
                }
                $frame->thumbnailImage($this->height, $this->width, $forceResize);
                $frame->setImageDelay(50);
                $this->gif->addimage($frame);

            } catch (ImagickException $e) {
                echo "Line {$e->getLine()} :{$e->getMessage()}";
                exit;
            }
        }

        try{
            $this->gif->writeImages($this->filePath, true);
        } catch (ImagickException $e) {
            echo "Line {$e->getLine()} :{$e->getMessage()}";
            exit;
        }
    }

    /**
     * Create thumbnail from generated gif
     *
     * @param $thumbPath
     * @param $height
     * @param $width
     * @return null
     */
    public function createThumbnail($thumbPath, $height, $width){
        if(!file_exists($this->getFilePath())){
            return null;
        }
        $gif = new Imagick($this->getFilePath());
        foreach($gif as $frame){
            $frame->scaleImage($width, $height);
        }
        $gif->writeImages($thumbPath, true);
        return $thumbPath;
    }

    /**
     * Resize the imagick image
     *
     * @param Imagick $imagick
     * @param String $gravity
     * @return Imagick
     */
    private function _resizeImagick(Imagick $imagick, $gravity=null){
        $originalImageHeight = $imagick->getImageHeight();
        $originalImageWidth  = $imagick->getImageWidth();
        if($originalImageHeight !== $this->height || $originalImageWidth !== $this->width){
            $heightDiff = ($originalImageHeight > $originalImageWidth) ? $originalImageHeight - $originalImageWidth : 0;

            switch($gravity){
                case 'CENTER':
                    $imagick->cropImage($originalImageWidth, $originalImageHeight-$heightDiff, 0, $heightDiff/2);
                    break;
                case 'TOP':
                    $imagick->cropImage($originalImageWidth, $originalImageHeight-$heightDiff, 0, 0);
                    break;
                case 'BOTTOM':
                    $imagick->cropImage($originalImageWidth, $originalImageHeight-$heightDiff, 0, $heightDiff);
                    break;
                default:
                    //Default center
                    $imagick->cropImage($originalImageWidth, $originalImageHeight-$heightDiff, 0, $heightDiff/2);
                    break;
            }
        }
        return $imagick;
    }

    /**
     * Takes remote url and saves overlay with a return path
     *
     * @param $overlayUrl
     * @return string
     */
    private function _buildOverlayPath($overlayUrl){
        $urlPieces = explode('.', $this->filePath);
        $path = "{$urlPieces[0]}_overlay.jpg";
        file_put_contents($path, file_get_contents($overlayUrl));
        return $path;
    }

    /**
     * Builds a filepath for a single gif frame
     *
     * @param $frame
     * @return string
     */
    private function _buildFramePath($frame){
        $urlPieces = explode('.', $this->filePath);
        $path = "{$urlPieces[0]}_{$frame}.jpg";
        $this->addFrame($path);
        return $path;
    }

    /**
     * Filepath to overlay on the image
     *
     * @param $overlay
     * @return $this
     */
    public function setOverlay($overlay){
        $this->overlay = $overlay;
        return $this;
    }

    /**
     * Set height
     *
     * @param $height
     * @return $this
     */
    public function setHeight($height){
        $this->height = $height;
        return $this;
    }

    /**
     * Set width
     *
     * @param $width
     * @return $this
     */
    public function setWidth($width){
        $this->width=$width;
        return $this;
    }

    /**
     * Boolean save frames
     *
     * @param $save
     * @return $this
     */
    public function saveFrames($save){
        $this->saveFrames = $save;
        return $this;
    }

    /**
     * Get image frames
     *
     * @return array
     */
    public function getFrames(){
        return $this->frames;
    }

    /**
     * Get a single frame file path
     * Starts at 0
     *
     * @param $position
     * @return string
     */
    public function getSingleFrame($position){
        return isset($this->frames[$position]) ? $this->frames[$position] : null;
    }

    /**
     * Add frame to end or at position
     *
     * @param $frame
     * @param null $position
     * @return $this
     */
    public function addFrame($frame, $position=null){
        if($position){
            $this->frames[$position] = $frame;
        } else {
            $this->frames[] = $frame;
        }
        return $this;
    }

    /**
     * Destroy the frames
     *
     * @return $this
     */
    public function destroyFrames(){
        if(!empty($this->frames)){
            foreach($this->frames as $frame){
                unlink($frame);
            }
        }
        return $this;
    }

    /**
     * Destroy Gif
     *
     * @return $this
     */
    public function destroyGif(){
        if(file_exists($this->filePath)){
            unlink($this->filePath);
        }
        return $this;
    }

    /**
     * Set the file save location
     *
     * @param $filePath
     * @return $this
     */
    public function setFilePath($filePath){
        $this->filePath = $filePath;
        return $this;
    }

    /**
     * Get file path
     *
     * @return mixed
     */
    public function getFilePath(){
        return $this->filePath;
    }

    /**
     * Checks to see if file path exists
     * otherwise path is null
     *
     * @param $file
     * @return null
     */
    private function _findFile($file){
        return file_exists($file) ? $file : null;
    }



}

<?php

namespace GifnHelper;

use OpenCloud\Rackspace;
use Exception;
use PDO;
use PDOException;


/**
 * Class CdnHelper
 *
 * @package Helpers
 * @author Adam Boerema
 */
class CdnHelper{

    /**
     * Extension constants
     */
    const LOGO_EXTENSION = '.logo.png';
    const OVERLAY_EXTENSION = '.mask.png';
    const OG_EXTENSION= '.og.png';
    const GIF_EXTENSION = '.gif';
    const MP4_EXTENSION = '.mp4';
    const THUMB_EXTENSION = '.thumb.gif';
    const FIRST_EXTENSION = '.first.jpg';
    const AD_EXTENSION = '.png';
    const ASSET_EXTENSION = '.png';

    /**
     * @var \OpenCloud\ObjectStore\Service
     */
    protected $service;

    /**
     * @var \OpenCloud\ObjectStore\Resource\Container
     */
    protected $container;

    /**
     * @var Rackspace
     */
    protected $client;

    /**
     * @var PDO
     */
    protected $db;


    /**
     * Construct
     *
     * @param $username
     * @param $apiKey
     * @param array $dbConfig {'host', 'db', 'user', 'password'}
     * @throws Exception
     */
    public function __construct($username, $apiKey, array $dbConfig){
        $secret = array(
            'username' => $username,
            'apiKey' => $apiKey
        );

        //Set the request and curl timeouts to 10 minutes
        $config = array(
            'curl.options' => array(
                CURLOPT_CONNECTTIMEOUT => 600,
                CURLOPT_TIMEOUT => 600
            ),
            'request.options' => array(
                'timeout' => 600,
                'connect_timeout' => 600
            )
        );
        try{
            $this->client = new Rackspace(Rackspace::US_IDENTITY_ENDPOINT, $secret, $config);
            $this->db = new PDO(
                "mysql:host={$dbConfig['host']};dbname={$dbConfig['db']}",
                $dbConfig['user'],
                $dbConfig['password']
            );
        } catch(Exception $e){
            throw $e;
        }

        $this->service = $this->client->objectStoreService('cloudFiles', 'DFW');
        $this->container = $this->service->getContainer('GIFN');
    }


    /**
     * Saves the array of file names and locations
     * to the CDN
     *
     * array containing arrays in format of
     *
     * array(
     *   name => "test.jpg",
     *   body => "/path/to/file"
     * )
     *
     * @param array $files
     * @throws exception
     */
    public function saveFiles(array $files){
        try{
            foreach($files as $file){
                $this->container->uploadObjects(array($file));
            }
        } catch (Exception $e){
            throw $e;
        }

    }


    /**
     * Updates files
     * array of arrays in format:
     *
     * array(
     *   name => "test.jpg",
     *   body => "/path/to/file"
     * )
     *
     * @param array $files
     */
    public function updateFiles(array $files){
        foreach($files as $file){
            $object = $this->container->getObject($file['name']);
            if($object){
                $object->delete();
            }
        }
    }

    /**
     * GetLogo
     *
     * @param $event
     * @return \OpenCloud\ObjectStore\Resource\DataObject
     */
    public function getLogo($event){
        $timestamp = $this->_getAssetTimestamp($event, 'logo');
        try{
            return $this->container->getObject(
                $this->buildFileName(self::LOGO_EXTENSION, $event, null, $timestamp)
            );
        } catch (Exception $e){
            return null;
        }
    }

    /**
     * @param $event
     * @param $file
     * @throws Exception
     */
    public function setLogo($event, $file){
        $object = $this->getLogo($event);
        if($object){
            $object->delete();
        }
        try{
            $timestamp = $this->_setAssetTimestamp($event, 'logo');
            $this->saveFiles(
                array(
                    array(
                        'name' => $this->buildFileName(self::LOGO_EXTENSION, $event, null, $timestamp),
                        'body' => fopen($file, 'r+')
                    )
                )
            );
        } catch (Exception $e){
            throw $e;
        }
    }

    /**
     * Get Overlay
     *
     * @param $event
     * @return \OpenCloud\ObjectStore\Resource\DataObject
     */
    public function getOverlay($event){
        $timestamp = $this->_getAssetTimestamp($event, 'overlay');
        try{
            return $this->container->getObject(
                $this->buildFileName(self::OVERLAY_EXTENSION, $event, null , $timestamp)
            );
        } catch (Exception $e){
            return null;
        }
    }

    /**
     * Set Overlay
     *
     * @param $event
     * @param $file
     * @throws Exception
     */
    public function setOverlay($event, $file){
        $object = $this->getOverlay($event);
        if($object){
            $object->delete();
        }
        try{
            $timestamp = $this->_setAssetTimestamp($event, 'overlay');
            $this->saveFiles(
                array(
                    array(
                        'name' =>   $this->buildFileName(self::OVERLAY_EXTENSION, $event, null, $timestamp),
                        'body' => fopen($file, 'r+')
                    )
                )
            );
        } catch (Exception $e){
            throw $e;
        }

    }

    /**
     * Get Overlay
     *
     * @param $event
     * @return \OpenCloud\ObjectStore\Resource\DataObject
     */
    public function getOG($event){
        $timestamp = $this->_getAssetTimestamp($event, 'og');
        try{
            return $this->container->getObject(
                $this->buildFileName(self::OG_EXTENSION, $event, null, $timestamp)
            );
        } catch (Exception $e){
            return null;
        }
    }

    /**
     * Set Overlay
     *
     * @param $event
     * @param $file
     * @throws Exception
     */
    public function setOG($event, $file){
        $object = $this->getOG($event);
        if($object){
            $object->delete();
        }

        try{
            $timestamp = $this->_setAssetTimestamp($event, 'og');
            $this->saveFiles(
                array(
                    array(
                        'name' => $this->buildFileName(self::OG_EXTENSION, $event, null, $timestamp),
                        'body' => fopen($file, 'r+')
                    )
                )
            );
        } catch (Exception $e){
            throw $e;
        }

    }

    /**
     * Get Gif
     *
     * @param $event
     * @param $slug
     * @return \OpenCloud\ObjectStore\Resource\DataObject
     */
    public function getGif($event, $slug){
        try{
            return $this->container->getObject(
                $this->buildFileName(self::GIF_EXTENSION, $event, $slug)
            );
        } catch (Exception $e){
            return null;
        }
    }

    /**
     * Set Gif
     *
     * @param $event
     * @param $slug
     * @param $file
     * @throws Exception
     */
    public function setGif($event, $slug, $file){
        $object = $this->getGif($event, $slug);
        if($object){
            $object->delete();
        }

        try{
            $this->saveFiles(
                array(
                    array(
                        'name' =>   $this->buildFileName(self::GIF_EXTENSION, $event, $slug),
                        'body' => fopen($file, 'r+')
                    )
                )
            );
        } catch (Exception $e){
            throw $e;
        }

    }

    /**
     * Get Thumb
     *
     * @param $event
     * @param $slug
     * @return \OpenCloud\ObjectStore\Resource\DataObject
     */
    public function getThumb($event, $slug){
        try{
            return $this->container->getObject(
                $this->buildFileName(self::THUMB_EXTENSION, $event, $slug)
            );
        } catch (Exception $e){
            return null;
        }
    }

    /**
     * Set Thumb
     *
     * @param $event
     * @param $slug
     * @param $file
     * @throws Exception
     */
    public function setThumb($event, $slug, $file){
        $object = $this->getThumb($event, $slug);
        if($object){
            $object->delete();
        }

        try{
            $this->saveFiles(
                array(
                    array(
                        'name' =>   $this->buildFileName(self::THUMB_EXTENSION, $event, $slug),
                        'body' => fopen($file, 'r+')
                    )
                )
            );
        } catch (Exception $e){
            throw $e;
        }

    }

    /**
     * Get First Frame
     *
     * @param $event
     * @param $slug
     * @return \OpenCloud\ObjectStore\Resource\DataObject
     */
    public function getFirstFrame($event, $slug){
        try{
            return $this->container->getObject(
                $this->buildFileName(self::FIRST_EXTENSION, $event, $slug)
            );
        } catch (Exception $e){
            return null;
        }
    }

    /**
     * Set First Frame
     *
     * @param $event
     * @param $slug
     * @param $file
     * @throws Exception
     */
    public function setFirstFrame($event, $slug, $file){
        $object = $this->getFirstFrame($event, $slug);
        if($object){
            $object->delete();
        }

        try{
            $this->saveFiles(
                array(
                    array(
                        'name' =>   $this->buildFileName(self::FIRST_EXTENSION, $event, $slug),
                        'body' => fopen($file, 'r+')
                    )
                )
            );
        } catch (Exception $e){
            throw $e;
        }

    }

    /**
     * Get Mp4
     *
     * @param $event
     * @param $slug
     * @return \OpenCloud\ObjectStore\Resource\DataObject
     */
    public function getMp4($event, $slug){
        try{
            return $this->container->getObject(
                $this->buildFileName(self::MP4_EXTENSION, $event, $slug)
            );
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Set Mp4
     *
     * @param $event
     * @param $slug
     * @param $file
     * @throws Exception
     */
    public function setMp4($event, $slug, $file){
        $object = $this->getMp4($event, $slug);
        if($object){
            $object->delete();
        }
        try{
            $this->saveFiles(
                array(
                    array(
                        'name' =>   $this->buildFileName(self::MP4_EXTENSION, $event, $slug),
                        'body' => fopen($file, 'r+')
                    )
                )
            );
        }
        catch (Exception $e){
            throw $e;
        }
    }

    /**
     * Sets the advertisement
     *
     * @param $name
     * @param $file
     * @throws Exception
     */
    public function setAd($name, $file){
        $object = $this->getAd($name);
        if($object){
            $object->delete();
        }
        try{
            $this->saveFiles(
                array(
                    array(
                        'name' =>   $this->buildAdName(self::AD_EXTENSION, $name),
                        'body' => fopen($file, 'r+')
                    )
                )
            );
        } catch (Exception $e){
            throw $e;
        }
    }

    /**
     * Retrieves the Advertisement
     *
     * @param $name - name with hashed timestamp
     * @return null|\OpenCloud\ObjectStore\Resource\DataObject
     */
    public function getAd($name){
        try{
            return $this->container->getObject(
                $this->buildAdName(self::AD_EXTENSION, $name)
            );
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Get asset
     *
     * @param $name
     * @return null|\OpenCloud\ObjectStore\Resource\DataObject
     */
    public function getAsset($name){
        try{
            return $this->container->getObject(
                $this->buildAdName(self::AD_EXTENSION, $name)
            );
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Set Asset
     *
     * @param $name
     * @param $file
     * @throws Exception
     */
    public function setAsset($name, $file){
        $object = $this->getAsset($name);
        if($object){
            $object->delete();
        }
        try{
            $this->saveFiles(
                array(
                    array(
                        'name' =>   $this->buildAssetName(self::ASSET_EXTENSION, $name),
                        'body' => fopen($file, 'r+')
                    )
                )
            );
        } catch (Exception $e){
            throw $e;
        }
    }


    /**
     * Builds the Rackspace object
     *
     * @return Rackspace
     */
    public function getContainer(){
        return $this->container;
    }

    /**
     * Builds the file name for ads
     *
     * @param $extension
     * @param $name
     * @return string
     */
    public function buildAdName($extension, $name){
        return "support/{$name}{$extension}";
    }

    /**
     * Build asset name
     *
     * @param $extension
     * @param $name
     * @return string
     */
    public function buildAssetName($extension, $name){
        return "assets/{$name}{$extension}";
    }

    /**
     * Helper for building the filenames
     *
     * @param $extension
     * @param $event
     * @param null $slug
     * @param null $timestamp
     * @return string
     */
    public function buildFileName($extension, $event, $slug = null, $timestamp = null){
        if($slug){
            return "{$event}/gif/{$slug}{$extension}";
        } elseif($timestamp) {
            return "{$event}/{$event}_{$timestamp}{$extension}";
        } else {
            return "{$event}/{$event}{$extension}";
        }
    }

    /**
     * Get all files in an event
     *
     * @param $event
     * @return array
     */
    public function getEventFiles($event){
        $containerObjects = array();
        $marker = '';
        while($marker !== null){
            $params = array(
                'marker' => $marker,
                'prefix' => "$event/gif/"
            );

            $objects = $this->container->objectList($params);
            $total = $objects->count();
            $count = 0;

            if ($total == 0) {
                break;
            }

            foreach ($objects as $object) {
                $containerObjects[] = $object->getName();
                $count++;

                $marker = ($count == $total) ? $object->getName() : null;
            }
        }

        return $containerObjects;
    }

    /**
     * Get the asset timestamp
     *
     * @param $eventCode
     * @param $asset
     * @throws \PDOException
     * @return integer $time
     */
    private function _getAssetTimestamp($eventCode, $asset){
        $time = null;
        try{
            $statement = $this->db->prepare("SELECT $asset FROM event_asset_cache
                                             WHERE event_id = (SELECT id from event where code = :code)");
            $statement->execute(array('code' => $eventCode));
            $time = $statement->fetch();
        } catch (PDOException $e){
            throw $e;
        }
        return isset($time[$asset]) ? $time[$asset] : null;
    }


    /**
     * Sets the unix timestamp for the event asset and returns
     * the timestamp
     *
     * @param $eventCode
     * @param $asset
     * @throws Exception
     * @return integer unix timestamp
     */
    private function _setAssetTimestamp($eventCode, $asset){
        $time = time();
        try{
            $statement = $this->db->prepare("INSERT INTO event_asset_cache (event_id, $asset)
                                             VALUES ((SELECT id from event where code = :code), :assetValue)
                                             ON DUPLICATE KEY UPDATE $asset= :assetValue");
            $statement->execute(array(
                    'code' => $eventCode,
                    'assetValue' => $time
                )
            );
        } catch (PDOException $e){
            throw $e;
        }
        return $time;
    }
}